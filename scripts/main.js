﻿$(document).ready(function () {
    $.getJSON("./naoi/naoi.json", function (json) {
        prosthikiNaon(json);
    });
});

function prosthikiNaon(jsonObj) {
    var templateNaos = $(".naos-group-template");
    var container = $(".main-container");
    for (var i = 0; i < jsonObj.naoi.length; i++) {
        var naosJson = jsonObj.naoi[i];
        var naos = templateNaos.clone();

        naos.find(".group-title").html(naosJson.onomaNaou);
        naos.find(".group-text").html(naosJson.onomaYpefthinou);
        naos.find(".phone").html(naosJson.tilefonoYpefthinou);

        $(".main-container").append(naos.html());
    }
}

function apostoliSMS(owner) {
    var naos = $(owner).find(".group-title").text();
    var ypefthinos = $(owner).find(".group-text").text();
    var phone = $(owner).find(".phone").text();

    var message = "EXETE ENA NEO FAX";

    var confirmMsg = "Για τον " + naos + ",\nθα ειδοποιηθεί ο " + ypefthinos + " στο " + phone + ".\nΘέλετε να συνεχίσετε;";
    if (confirm(confirmMsg))
        postSMS(phone, message, function () {
            $(owner).addClass("inactive");
            setTimeout(function() {
                $(owner).removeClass("inactive");
            }, 60 * 60 * 1000);//θα κληθεί μετά από μία ώρα
        });
}

function getParameterizedUrl(phoneTo, message) {
    var user = "ggardikis@gmail.com";
    var pass = "niko30ta";
    var url = "http://www.smsbox.gr/httpapi/sendsms.php?username={0}&password={1}&from=NIKO56FAX&to={2}&text={3}";
    url = url.replace("{0}", user).replace("{1}", pass).replace("{2}", phoneTo).replace("{3}", message);

    return url;
}

function postSMS(phone, message, callback) {
    var url = getParameterizedUrl(phone, message);
    $.ajax({

        url: url,
        type: 'GET',
        crossDomain: true,
        dataType: 'jsonp',
        success: function () {
            callback();
        },
        error: function () {
            callback();
        },
    });
}

function anazitisiNaon() {
    var searchVal = $(".search-text").val();
    $(".main-container .naos-group").css("display", "");
    if (isEmptyOrSpaces(searchVal))
        return;

    searchTerms = searchVal.split(" ");
    $.each($(".main-container .naos-group"), function (index, value) {
        for (var i = 0; i < searchTerms.length; i++) {
            var term = searchTerms[i];
            if (!periecheiToKeimeno(value, term))
                $(value).css("display", "none");
        }
    });
}

function periecheiToKeimeno(htmlNaos, searchTerm) {
    var naos = $(htmlNaos).find(".group-title").text().toLowerCase();
    var ypefthinos = $(htmlNaos).find(".group-text").text().toLowerCase();
    var phone = $(htmlNaos).find(".phone").text().toLowerCase();

    var terms = naos + " " + ypefthinos + " " + phone;

    return terms.indexOf(searchTerm.toLowerCase()) > -1;
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}